# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=juk
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://juk.kde.org/"
pkgdesc="A jukebox, tagger and music collection manager"
license="GPL-2.0-or-later"
depends="phonon-backend-gstreamer"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	ktextwidgets-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	phonon-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	taglib-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/juk-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a810479ad43d9410419a928f18c6e45c531146d715841bc46a8ef0714b0a7e69a3534f0c22bfaba482a6277912900bbe3d1daad18337039ac21a88e6b4473eb3  juk-22.04.1.tar.xz
"
