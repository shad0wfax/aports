# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kajongg
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kconfigwidgets
arch="noarch !armhf !s390x !riscv64"
url="https://kde.org/applications/games/org.kde.kajongg"
pkgdesc="Mah Jongg - the ancient Chinese board game for 4 players"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="
	python3
	py3-twisted
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kdoctools-dev
	libkmahjongg-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kajongg-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
815b7c06f750f46abae6bfa4b747c9c77225a03f001e597a10fcd14e17459eafd988f39af48158e686a3ed5e3734637567c45678ce8d7e23ae8164c582e72cd0  kajongg-22.04.1.tar.xz
"
