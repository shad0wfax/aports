# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ktorrent
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by polkit -> plasma-workspace
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/internet/org.kde.ktorrent"
pkgdesc="A powerful BitTorrent client for KDE"
license="GPL-2.0-or-later"
makedepends="
	boost-dev
	extra-cmake-modules
	karchive-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdnssd-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemviews-dev
	knotifications-dev
	knotifyconfig-dev
	kparts-dev
	kplotting-dev
	kross-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libktorrent-dev
	phonon-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	samurai
	solid-dev
	syndication-dev
	taglib-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktorrent-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	# The infowidget plugin is disabled due to an incompatibility with musl
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DENABLE_INFOWIDGET_PLUGIN=FALSE
	cmake --build build
}

check() {
	cd build

	# ipblocklisttest requires itself installed
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "ipblocklisttest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bea2a018383452e8871363aa4869e10ac1bcc5d99145f845c67405d0bc970e2081a0e98b8cd8fe6f23178285287c128fec677438c466b62ac11c4c4cd16fce93  ktorrent-22.04.1.tar.xz
"
