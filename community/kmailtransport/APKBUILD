# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmailtransport
pkgver=22.04.1
pkgrel=0
pkgdesc="Manage mail transport"
# armhf blocked by extra-cmake-modules
# ppc64le and s390x blocked by libkgapi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="
	akonadi-dev
	akonadi-mime-dev
	kcmutils-dev
	kconfigwidgets-dev
	ki18n-dev
	kio-dev
	kmime-dev
	ksmtp-dev
	kwallet-dev
	libkgapi-dev
	qt5-qtkeychain-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmailtransport-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Requires OpenGL and running dbus

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
94da43c0b9649dd556391794eaf733802586598b26c854ba8960b291fda45712cda35ff162181471784496ba2e5b8a3f49b22a56e070b867c8f2955588dde3b1  kmailtransport-22.04.1.tar.xz
"
