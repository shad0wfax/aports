# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=dolphin-plugins
pkgver=22.04.1
pkgrel=0
url="https://www.kde.org/applications/system/dolphin/"
pkgdesc="Extra Dolphin plugins"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
license="GPL-2.0-or-later"
makedepends="
	dolphin-dev
	extra-cmake-modules
	ki18n-dev
	kio-dev
	ktexteditor-dev
	ktextwidgets-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/dolphin-plugins-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_svn=OFF # Broken "error: expected unqualified-id before '(' token"
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0833311429857a6aeccb19736ac2d49e8b4e0e9f9d393b2835b33f8f17032382d07fbcf61dd03d37dccebcaea1b3a542c5478f24dd47b52749bd1259461caa2f  dolphin-plugins-22.04.1.tar.xz
"
