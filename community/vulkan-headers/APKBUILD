# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: Simon Zeni <simon@bl4ckb0ne.ca>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=vulkan-headers
# Please be VERY careful upgrading this - vulkan-headers breaks API even
# on point releases. So please make sure everything using this still builds
# after upgrades
pkgver=1.3.211.0
pkgrel=0
arch="noarch"
url="https://www.vulkan.org/"
pkgdesc="Vulkan header files"
license="Apache-2.0"
makedepends="cmake"
source="https://github.com/khronosgroup/vulkan-headers/archive/sdk-$pkgver/vulkan-headers-sdk-$pkgver.tar.gz"
options="!check" # No tests
builddir="$srcdir/Vulkan-Headers-sdk-$pkgver"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
35f849b0c493e6e1ecfeb91089085f3a567cce76466f8cc17acadfa2a2b3163420489c11fc5b547de52aed1575f28fc23c1a62ce6d321ccba3dd04fbf7e50ca3  vulkan-headers-sdk-1.3.211.0.tar.gz
"
