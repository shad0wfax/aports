# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-astroid
pkgver=2.11.5
pkgrel=0
pkgdesc="A new abstract syntax tree from Python's ast"
url="https://pylint.pycqa.org/projects/astroid/en/latest/"
arch="noarch"
license="LGPL-2.1-or-later"
depends="python3 py3-lazy-object-proxy py3-wrapt"
replaces="py-logilab-astng"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-typing-extensions"
source="py3-astroid-$pkgver.tar.gz::https://github.com/PyCQA/astroid/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/astroid-$pkgver"

build() {
	python3 setup.py build
}

check() {
	pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
6b6d1e35b95c68d7219534ceda6e2802cc6f2d9cd9bed6a880eaf0937dfb288d9fb58dac9be33072f4676a568f778eb26a78f447d4a44842a98d8677e151e1bb  py3-astroid-2.11.5.tar.gz
"
